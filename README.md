


![vital-5g-logo](https://www.vital5g.eu/wp-content/uploads/2020/12/vital-logo-web.png) 
# Multi-site Inventory


[VITAL-5G D2.1](https://www.vital5g.eu/wp-content/uploads/2022/01/VITAL5G_D2.1_Initial_NetApps_blueprints_and_Open_Repository_design_Final.pdf) 

*The Multi-Site Inventory is a management backend software component that stores the information related to the systems, platforms, infrastructure capabilities and devices installed into the three VITAL-5G testbeds and exposes these details to the other internal components of the VITAL-5G Platform*

## Software architecture
The following figure illustrates the software architecture of this module, highlighting the components currently supported

![vital-5g-multisite-inventory-architecture](docs/vital-5g-multisite-inventory-architecture.png)

The source code is available in the [SRC](src/) folder of the repository. The code is structured in two different maven projects:
* *vital-5g-multisite-inventory-interfaces* : Containing the Java  based interfaces and and classes representing the information models of the instances.
* *vital-5g-multisite-inventory-lcm*: Containing the implementation of the rest controllers, the logic for instance management and driver for the interaction with the rest of the components of the platform.

## Deployment 

A docker-compose based deployment is detailed in [Readme](installation/)

## Folder structure
* [src](src/): Contains the source code of this module
* [API](API/): OpenAPI specification of the interfaces by this module and Postman collections
* [docs](docs/): Documentation of the module, including example descriptors using during the integration tests  

## License
This module has been developed by [Nextworks](www.nextworks.it) and licensed under the open source [Apache License v2.0](https://www.apache.org/licenses/LICENSE-2.0)
