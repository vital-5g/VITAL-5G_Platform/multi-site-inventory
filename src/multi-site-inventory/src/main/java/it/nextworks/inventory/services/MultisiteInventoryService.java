package it.nextworks.inventory.services;

import it.nextworks.inventory.elements.Environment;
import it.nextworks.inventory.elements.Equipment;
import it.nextworks.inventory.elements.Testbed;
import it.nextworks.inventory.elements.TestbedNfvoInformation;
import it.nextworks.inventory.elements.monitoring.MonitoringInfo;
import it.nextworks.inventory.elements.slicing.SlicingInfo;
import it.nextworks.inventory.exceptions.NotExistingEntityException;
import it.nextworks.inventory.interfaces.MultiSiteInventoryInterface;
import it.nextworks.inventory.nbi.MultisiteInventoryRestController;
import it.nextworks.inventory.repos.*;
import it.nextworks.inventory.repos.TestbedEquipmentRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class MultisiteInventoryService  implements MultiSiteInventoryInterface {

    private static final Logger log = LoggerFactory.getLogger(MultisiteInventoryService.class);

    @Autowired
    private TestbedNfvoInformationRepository testbedNfvoInformationRepository;

    @Autowired
    private TestbedMonitoringInformationRepository testbedMonitoringInformationRepository;
    @Autowired
    private TestbedEquipmentRepository testbedEquipmentRepository;

    @Autowired
    private TestbedSlicingInformationRepository testbedSlicingInformationRepository;

    @Override
    public List<TestbedNfvoInformation> getNfvoInformation(Testbed testbed, Environment environment) {
        if(environment!=null){
            return testbedNfvoInformationRepository.findByTestbedAndEnvironment(testbed, environment);
        }else return testbedNfvoInformationRepository.findByTestbed(testbed);

    }

    @Override
    public UUID onboardTestbedNfvo(TestbedNfvoInformation nfvoInformation) {
        log.debug("Onboarding new NFVO");

        testbedNfvoInformationRepository.saveAndFlush(nfvoInformation);
        return nfvoInformation.getNfvoRecordId();
    }

    public void deleteTestbedNfvo(Testbed testbed, UUID nfvoRecordId) throws NotExistingEntityException {
        log.debug("Received request to delete NFVO record with ID:"+nfvoRecordId);
        Optional<TestbedNfvoInformation> info = testbedNfvoInformationRepository.findById(nfvoRecordId);
        if(info.isPresent()){
            if(testbed.equals(info.get().getTestbed())){
                testbedNfvoInformationRepository.delete(info.get());
            }else throw new NotExistingEntityException("Could not find NFVO with specified ID for specified testbed");

        }else throw new NotExistingEntityException("Could not find NFVO with specified ID");
    }

    @Override
    public UUID onboardTestbedEquipment(Equipment equipment) {
        log.debug("Onboarding equipment:"+equipment.getEquipmentId()+" on testbed "+equipment.getTestbed());
        Optional<Equipment> currentE = testbedEquipmentRepository.findByTestbedAndEquipmentId(equipment.getTestbed(), equipment.getEquipmentId());
        if(currentE.isPresent()){
            //TODO
            log.debug("Equipment with same ID alrady onboarding. IGNORING");
            return currentE.get().getEquipmentRecordId();
        }else{
            testbedEquipmentRepository.saveAndFlush(equipment);
            log.debug("Onboarded new equipment with record ID:"+equipment.getEquipmentRecordId());
            return equipment.getEquipmentRecordId();
        }
    }

    @Override
    public List<Equipment> getEquipment(Testbed testbed) {
        log.debug("Retrieving equipment on testbed "+testbed);
        return testbedEquipmentRepository.findByTestbed(testbed);
    }



    @Override
    public Equipment getEquipment(Testbed testbed, UUID equipmentId) throws  NotExistingEntityException{
        log.debug("Retrieving equipment on testbed {} with ID {} ",testbed,equipmentId);
        Optional<Equipment> equipment = testbedEquipmentRepository.findByEquipmentRecordIdAndTestbed(equipmentId, testbed);
        if(equipment.isPresent()){
            return equipment.get();
        }else throw new NotExistingEntityException("Could not find equipment with given ID and testbed");

    }

    @Override
    public UUID onboardTestbedSlicingInfo(SlicingInfo info) {
        log.debug("Onboarding Testbed Slicing Info "+info.getTestbed());
        Optional<SlicingInfo> oldInfo = testbedSlicingInformationRepository.findByTestbed(info.getTestbed());
        if(oldInfo.isPresent()){
            log.debug("Deleting of Slicing Info record");
            testbedSlicingInformationRepository.delete(oldInfo.get());
        }

        testbedSlicingInformationRepository.saveAndFlush(info);
        return info.getSlicingInfoRecordId();
    }

    @Override
    public SlicingInfo getTestbedSlicingInfo(Testbed testbed) throws NotExistingEntityException {
        log.debug("Retrieving Testbed Slicing Info "+testbed);
        Optional<SlicingInfo> oldInfo = testbedSlicingInformationRepository.findByTestbed(testbed);
        if(oldInfo.isPresent()){
            return oldInfo.get();
        }else throw  new NotExistingEntityException("Could not find slicing info for testbed "+testbed);
    }

    @Override
    public UUID onboardTestbedMonitoringInfo(MonitoringInfo info) {
        log.debug("Onboarding Testbed Monitoring Info "+info.getTestbed());
        Optional<MonitoringInfo> oldInfo = testbedMonitoringInformationRepository.findByTestbed(info.getTestbed());
        if(oldInfo.isPresent()){
            log.debug("Deleting of Monitoring Info record");
            testbedMonitoringInformationRepository.delete(oldInfo.get());
        }

        testbedMonitoringInformationRepository.saveAndFlush(info);
        return info.getMonitoringInfoRecordId();
    }

    @Override
    public MonitoringInfo getTestbedMonitoringInfo(Testbed testbed) throws NotExistingEntityException {
        log.debug("Retrieving Testbed Monitoring Info "+testbed);
        Optional<MonitoringInfo> oldInfo = testbedMonitoringInformationRepository.findByTestbed(testbed);
        if(oldInfo.isPresent()){
            return oldInfo.get();
        }else throw  new NotExistingEntityException("Could not find Monitoring info for testbed "+testbed);
    }

    @Override
    public void deleteEquipment(Testbed testbed, UUID equipmentId) throws NotExistingEntityException {
        log.debug("Deleting equipment on  with ID {} ",equipmentId);
        Optional<Equipment> equipment = testbedEquipmentRepository.findByEquipmentRecordIdAndTestbed(equipmentId, testbed);
        if(equipment.isPresent()){
            testbedEquipmentRepository.delete(equipment.get());
        }else throw new NotExistingEntityException("Could not find equipment with given ID");
    }


}
