package it.nextworks.inventory.nbi;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import it.nextworks.inventory.elements.Environment;
import it.nextworks.inventory.elements.Equipment;
import it.nextworks.inventory.elements.Testbed;
import it.nextworks.inventory.elements.TestbedNfvoInformation;
import it.nextworks.inventory.elements.monitoring.MonitoringInfo;
import it.nextworks.inventory.elements.slicing.SlicingInfo;
import it.nextworks.inventory.exceptions.NotExistingEntityException;
import it.nextworks.inventory.services.MultisiteInventoryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.UUID;

@Api(tags = "Multisite Inventory API")
@RestController
@CrossOrigin
@RequestMapping("/multisite-inventory")

public class MultisiteInventoryRestController {

    private static final Logger log = LoggerFactory.getLogger(MultisiteInventoryRestController.class);

    @Autowired
    private MultisiteInventoryService multisiteInventoryService;

    @ApiOperation(value = "Onboard NFVO")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Onboarded a new NFVO.", response = UUID.class),
            @ApiResponse(code = 400, message = "The request contains elements impossible to process", response = ResponseEntity.class),
            @ApiResponse(code = 409, message = "There is a conflict with the request", response = ResponseEntity.class),
            @ApiResponse(code = 500, message = "Status 500", response = ResponseEntity.class)

    })
    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(value = {"/{testbed}/nfvos"}, method = RequestMethod.POST)
    public ResponseEntity<?> onboardNfvo(@PathVariable Testbed testbed, @RequestBody TestbedNfvoInformation nfvoInformation) {
        log.debug("Received request to Onboard a Testbed NFVO.");
        try {

            UUID nfvoId = multisiteInventoryService.onboardTestbedNfvo(nfvoInformation);
            return new ResponseEntity<UUID>(nfvoId, HttpStatus.CREATED);

        } catch (Exception e) {
            log.error("Internal exception", e);
            return new ResponseEntity<String>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }


    @ApiOperation(value = "Get a Testbed NFVOs")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Retrieve Testbed Nfvos."),

    })
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/{testbed}/nfvos", method = RequestMethod.GET)
    public ResponseEntity<?> getNfvos(@PathVariable Testbed testbed, @RequestParam Environment environment) {
        log.debug("Received request to retrieve Testbed NFVOs");
        try {
            List<TestbedNfvoInformation> out = multisiteInventoryService.getNfvoInformation(testbed, environment);

            return  new ResponseEntity(out, HttpStatus.OK);

        } catch (Exception e) {
            log.error("Internal exception", e);
            return new ResponseEntity<String>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @ApiOperation(value = "Delete Testbed NFVO")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Delete Testbed NFVO."),

    })
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/{testbed}/nfvos/{nfvoRecordId}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteNfvo(@PathVariable Testbed testbed, @PathVariable UUID nfvoRecordId) {
        log.debug("Received request to retrieve Testbed NFVOs");
        try {
            multisiteInventoryService.deleteTestbedNfvo(testbed, nfvoRecordId);

            return  new ResponseEntity(HttpStatus.OK);

        } catch (Exception e) {
            log.error("Internal exception", e);
            return new ResponseEntity<String>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }



    @ApiOperation(value = "Onboard Testbed Equipment")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Onboarded a new Equipment.", response = UUID.class),
            @ApiResponse(code = 400, message = "The request contains elements impossible to process", response = ResponseEntity.class),
            @ApiResponse(code = 409, message = "There is a conflict with the request", response = ResponseEntity.class),
            @ApiResponse(code = 500, message = "Status 500", response = ResponseEntity.class)

    })
    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(value = {"/{testbed}/equipment"}, method = RequestMethod.POST)
    public ResponseEntity<?> onboardEquipment(@PathVariable Testbed testbed, @RequestBody Equipment equipment) {
        log.debug("Received request to Onboard a Testbed Equipment .");
        try {

            UUID equipmentRecordId = multisiteInventoryService.onboardTestbedEquipment(equipment);
            return new ResponseEntity<UUID>(equipmentRecordId, HttpStatus.CREATED);

        } catch (Exception e) {
            log.error("Internal exception", e);
            return new ResponseEntity<String>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }


    @ApiOperation(value = "Get ALL Testbed Equipment")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Testbed Equipment List."),

    })
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/{testbed}/equipment", method = RequestMethod.GET)
    public ResponseEntity<?> getAllEquipment(@PathVariable Testbed testbed) {
        log.debug("Received request to retrieve the Equipment List.");
        try {
            List<Equipment> out = multisiteInventoryService.getEquipment(testbed);

            return  new ResponseEntity(out, HttpStatus.OK);

        } catch (Exception e) {
            log.error("Internal exception", e);
            return new ResponseEntity<String>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @ApiOperation(value = "Get Testbed Equipment")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Testbed Equipment."),

    })
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/{testbed}/equipment/{equipmentId}", method = RequestMethod.GET)
    public ResponseEntity<?> getEquipment(@PathVariable Testbed testbed, @PathVariable UUID equipmentId ) {
        log.debug("Received request to retrieve a Equipment.");
        try {
            Equipment out = multisiteInventoryService.getEquipment(testbed, equipmentId);

            return  new ResponseEntity(out, HttpStatus.OK);

        } catch (Exception e) {
            log.error("Internal exception", e);
            return new ResponseEntity<String>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }


    @ApiOperation(value = "Delete Testbed Equipment")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Testbed Equipment."),

    })
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/{testbed}/equipment/{equipmentId}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteEquipment(@PathVariable Testbed testbed, @PathVariable UUID equipmentId ) {
        log.debug("Received request to retrieve a Equipment.");
        try {
           multisiteInventoryService.deleteEquipment(testbed, equipmentId);

           return  new ResponseEntity(HttpStatus.OK);

        } catch (Exception e) {
            log.error("Internal exception", e);
            return new ResponseEntity<String>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }


    @ApiOperation(value = "Onboard Testbed Slicing Capabilities")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Onboarded Testbed Slicing.", response = UUID.class),
            @ApiResponse(code = 400, message = "The request contains elements impossible to process", response = ResponseEntity.class),
            @ApiResponse(code = 500, message = "Internal server error", response = ResponseEntity.class),

    })
    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(value = {"/{testbed}/slicing"}, method = RequestMethod.POST)
    public ResponseEntity<?> onboardSlicing(@PathVariable Testbed testbed, @RequestBody SlicingInfo slicingInformation) {
        log.debug("Received request to Onboard Slicing Info.");
        try {
            if(slicingInformation.getTestbed()!=null && !slicingInformation.getTestbed().equals(testbed)){
                return new ResponseEntity<String>("Testbeds specified in request do not match", HttpStatus.BAD_REQUEST);
            }
            slicingInformation.setTestbed(testbed);
            UUID nfvoId = multisiteInventoryService.onboardTestbedSlicingInfo(slicingInformation);
            return new ResponseEntity<UUID>(nfvoId, HttpStatus.CREATED);

        } catch (Exception e) {
            log.error("Internal exception", e);
            return new ResponseEntity<String>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }


    @ApiOperation(value = "Get Testbed Slicing capabilties")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Testbed Equipment List."),

    })
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/{testbed}/slicing", method = RequestMethod.GET)
    public ResponseEntity<?> getSlicing(@PathVariable Testbed testbed) {
        log.debug("Received request to Onboard Slicing Info.");
        try {

            SlicingInfo info = multisiteInventoryService.getTestbedSlicingInfo(testbed);
            return new ResponseEntity<SlicingInfo>(info, HttpStatus.OK);
        } catch (NotExistingEntityException e){
            return new ResponseEntity<String>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (Exception e) {
            log.error("Internal exception", e);
            return new ResponseEntity<String>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }



    @ApiOperation(value = "Onboard Testbed Monitoring Info")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Onboarded Testbed Monitoring.", response = UUID.class),
            @ApiResponse(code = 400, message = "The request contains elements impossible to process", response = ResponseEntity.class),
            @ApiResponse(code = 500, message = "Internal server error", response = ResponseEntity.class),

    })
    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(value = {"/{testbed}/monitoring"}, method = RequestMethod.POST)
    public ResponseEntity<?> onboardMonitoring(@PathVariable Testbed testbed, @RequestBody MonitoringInfo monitoringInformation) {
        log.debug("Received request to Onboard Monitoring Info.");
        try {
            if(monitoringInformation.getTestbed()!=null && !monitoringInformation.getTestbed().equals(testbed)){
                return new ResponseEntity<String>("Testbeds specified in request do not match", HttpStatus.BAD_REQUEST);
            }
            monitoringInformation.setTestbed(testbed);
            UUID id = multisiteInventoryService.onboardTestbedMonitoringInfo(monitoringInformation);
            return new ResponseEntity<UUID>(id, HttpStatus.CREATED);

        } catch (Exception e) {
            log.error("Internal exception", e);
            return new ResponseEntity<String>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }


    @ApiOperation(value = "Get Testbed Monitoring")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Testbed Equipment List."),

    })
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/{testbed}/monitoring", method = RequestMethod.GET)
    public ResponseEntity<?> getMontoring(@PathVariable Testbed testbed) {
        log.debug("Received request to Onboard Slicing Info.");
        try {

            MonitoringInfo info = multisiteInventoryService.getTestbedMonitoringInfo(testbed);
            return new ResponseEntity<MonitoringInfo>(info, HttpStatus.OK);
        } catch (NotExistingEntityException e){
            return new ResponseEntity<String>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (Exception e) {
            log.error("Internal exception", e);
            return new ResponseEntity<String>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }
}
