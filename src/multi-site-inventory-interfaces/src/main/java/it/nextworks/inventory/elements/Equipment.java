package it.nextworks.inventory.elements;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.UUID;

@Entity
public class Equipment {
    @Id
    @GeneratedValue
    private UUID equipmentRecordId;
    private AccessLevel accessLevel;
    private String equipmentId;
    private String location;
    private EquipmentType  type;
    private EquipmentSubType subType;
    private Testbed testbed;

    public Equipment() {
    }

    public Equipment(AccessLevel accessLevel, String equipmentId, String location, EquipmentType type, EquipmentSubType subType, Testbed testbed) {
        this.accessLevel = accessLevel;
        this.equipmentId = equipmentId;
        this.location = location;
        this.type = type;
        this.subType = subType;
        this.testbed= testbed;
    }

    public UUID getEquipmentRecordId() {
        return equipmentRecordId;
    }

    public AccessLevel getAccessLevel() {
        return accessLevel;
    }

    public String getEquipmentId() {
        return equipmentId;
    }

    public String getLocation() {
        return location;
    }

    public EquipmentType getType() {
        return type;
    }

    public EquipmentSubType getSubType() {
        return subType;
    }

    public Testbed getTestbed() {
        return testbed;
    }
}
