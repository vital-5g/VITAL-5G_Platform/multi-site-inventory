package it.nextworks.inventory.interfaces;

import it.nextworks.inventory.elements.Environment;
import it.nextworks.inventory.elements.Equipment;
import it.nextworks.inventory.elements.Testbed;
import it.nextworks.inventory.elements.TestbedNfvoInformation;
import it.nextworks.inventory.elements.monitoring.MonitoringInfo;
import it.nextworks.inventory.elements.slicing.SlicingInfo;
import it.nextworks.inventory.exceptions.NotExistingEntityException;

import java.util.List;
import java.util.UUID;

public interface MultiSiteInventoryInterface {

    List<TestbedNfvoInformation> getNfvoInformation(Testbed testbed, Environment environment);

    UUID onboardTestbedNfvo(TestbedNfvoInformation nfvoInformation);

    UUID onboardTestbedEquipment(Equipment equipment);

    List<Equipment> getEquipment(Testbed testbed);


    Equipment getEquipment(Testbed testbed, UUID equipmentId) throws NotExistingEntityException;


    UUID onboardTestbedSlicingInfo(SlicingInfo info);

    SlicingInfo getTestbedSlicingInfo(Testbed testbed) throws NotExistingEntityException;


    UUID onboardTestbedMonitoringInfo(MonitoringInfo info);

    MonitoringInfo getTestbedMonitoringInfo(Testbed testbed) throws NotExistingEntityException;


    void deleteEquipment(Testbed testbed, UUID equipmentId) throws NotExistingEntityException;
}
