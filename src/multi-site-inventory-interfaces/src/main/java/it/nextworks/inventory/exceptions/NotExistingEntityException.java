package it.nextworks.inventory.exceptions;

public class NotExistingEntityException extends Exception {
    public NotExistingEntityException(String s) {
        super(s);
    }
}
