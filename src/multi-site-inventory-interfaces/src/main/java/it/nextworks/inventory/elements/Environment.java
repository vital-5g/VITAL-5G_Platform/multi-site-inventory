package it.nextworks.inventory.elements;

public enum Environment {
    PRODUCTION,
    DEVELOPMENT
}
