package it.nextworks.inventory.elements.monitoring;

import it.nextworks.inventory.elements.Environment;
import it.nextworks.inventory.elements.Testbed;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.UUID;

@Entity
public class MonitoringInfo {
    @Id
    @GeneratedValue
    private UUID monitoringInfoRecordId;


    private String applicationEndpoint;
    private String infrastructureEndpoint;

    private Testbed testbed;
    private Environment environment;

    public MonitoringInfo() {
    }

    public MonitoringInfo(String applicationEndpoint, String infrastructureEndpoint, Testbed testbed, Environment environment) {
        this.applicationEndpoint = applicationEndpoint;
        this.infrastructureEndpoint = infrastructureEndpoint;
        this.testbed = testbed;
        this.environment = environment;
    }

    public String getApplicationEndpoint() {
        return applicationEndpoint;
    }

    public void setApplicationEndpoint(String applicationEndpoint) {
        this.applicationEndpoint = applicationEndpoint;
    }

    public String getInfrastructureEndpoint() {
        return infrastructureEndpoint;
    }

    public void setInfrastructureEndpoint(String infrastructureEndpoint) {
        this.infrastructureEndpoint = infrastructureEndpoint;
    }

    public Testbed getTestbed() {
        return testbed;
    }

    public void setTestbed(Testbed testbed) {
        this.testbed = testbed;
    }

    public Environment getEnvironment() {
        return environment;
    }

    public void setEnvironment(Environment environment) {
        this.environment = environment;
    }

    public UUID getMonitoringInfoRecordId() {
        return monitoringInfoRecordId;
    }
}
