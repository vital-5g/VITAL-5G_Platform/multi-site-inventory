package it.nextworks.inventory.elements;

public enum AccessLevel {

    READ,

    WRITE,
    READ_WRITE
}
