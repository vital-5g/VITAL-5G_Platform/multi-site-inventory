package it.nextworks.inventory.elements;

public enum EquipmentType {

    IOT_GW,
    SENSOR,
    ACTUATOR,
    TRANSPONDER,
    CONVERTER,
    CAMERA,
    GPS,
    AIS,
    LIDAR

}
