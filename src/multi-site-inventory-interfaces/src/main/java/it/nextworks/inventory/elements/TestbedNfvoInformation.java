package it.nextworks.inventory.elements;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.UUID;

@Entity
public class TestbedNfvoInformation {

    @Id
    @GeneratedValue

    private UUID nfvoRecordId;

    private String baseUrl;

    private UUID vimAccountId;

    private String username;

    private String password;

    private String project;

    private NfvoDriverType nfvoDriverType;

    private String vimEndpoint;
    private String vimExternalNetwork;
    private String vimUser;
    private String vimSecret;
    private String vimDomain;
    private Testbed testbed;

    private Environment environment;

    public TestbedNfvoInformation() {
    }

    public TestbedNfvoInformation(Testbed testbed, Environment environment, String baseUrl, UUID vimAccountId, String username, String password,
                                 NfvoDriverType nfvoDriverType, String project, String vimExternalNetwork, String vimUser, String vimEndpoint, String vimSecret, String vimDomain) {
        this.baseUrl = baseUrl;
        this.vimAccountId = vimAccountId;
        this.username = username;
        this.password = password;
        this.nfvoDriverType =nfvoDriverType;
        this.project = project;
        this.vimExternalNetwork = vimExternalNetwork;
        this.testbed=testbed;
        this.environment=environment;
        this.vimUser = vimUser;
        this.vimEndpoint=vimEndpoint;
        this.vimSecret=vimSecret;
        this.vimDomain=vimDomain;
    }

    public Environment getEnvironment() {
        return environment;
    }

    public Testbed getTestbed() {
        return testbed;
    }

    public NfvoDriverType getNfvoDriverType() {
        return nfvoDriverType;
    }

    public String getProject() {
        return project;
    }

    public String getBaseUrl() {
        return baseUrl;
    }

    public UUID getVimAccountId() {
        return vimAccountId;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getVimExternalNetwork() {
        return vimExternalNetwork;
    }

    public UUID getNfvoRecordId() {
        return nfvoRecordId;
    }

    public String getVimEndpoint() {
        return vimEndpoint;
    }

    public String getVimUser() {
        return vimUser;
    }

    public String getVimSecret() {
        return vimSecret;
    }

    public String getVimDomain() {
        return vimDomain;
    }
}
