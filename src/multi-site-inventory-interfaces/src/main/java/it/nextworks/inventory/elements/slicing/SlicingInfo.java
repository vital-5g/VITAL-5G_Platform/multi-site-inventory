package it.nextworks.inventory.elements.slicing;

import it.nextworks.inventory.elements.Testbed;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Entity
public class SlicingInfo {
    @Id
    @GeneratedValue
    private UUID slicingInfoRecordId;

    private boolean dynamicSliceModification;

    private boolean dynamicSliceProvisioning;

    private  boolean dynamicUEAttachment;


    private String slicingAgentUrl;
    private Testbed testbed;

    @ElementCollection
    private List<String> availableImsis = new ArrayList<>();


    public SlicingInfo() {
    }

    public SlicingInfo(boolean dynamicSliceModification, boolean dynamicSliceProvisioning, boolean dynamicUEAttachment, Testbed testbed,
                       String slicingAgentUrl, List<String> availableImsis) {
        this.dynamicSliceModification = dynamicSliceModification;
        this.dynamicSliceProvisioning = dynamicSliceProvisioning;
        this.dynamicUEAttachment = dynamicUEAttachment;
        this.testbed = testbed;
        this.slicingAgentUrl=slicingAgentUrl;
        if(availableImsis!=null) this.availableImsis=availableImsis;
    }

    public boolean isDynamicSliceModification() {
        return dynamicSliceModification;
    }

    public void setDynamicSliceModification(boolean dynamicSliceModification) {
        this.dynamicSliceModification = dynamicSliceModification;
    }

    public boolean isDynamicSliceProvisioning() {
        return dynamicSliceProvisioning;
    }

    public void setDynamicSliceProvisioning(boolean dynamicSliceProvisioning) {
        this.dynamicSliceProvisioning = dynamicSliceProvisioning;
    }

    public boolean isDynamicUEAttachment() {
        return dynamicUEAttachment;
    }

    public void setDynamicUEAttachment(boolean dynamicUEAttachment) {
        this.dynamicUEAttachment = dynamicUEAttachment;
    }

    public Testbed getTestbed() {
        return testbed;
    }

    public void setTestbed(Testbed testbed) {
        this.testbed = testbed;
    }

    public UUID getSlicingInfoRecordId() {
        return slicingInfoRecordId;
    }

    public String getSlicingAgentUrl() {
        return slicingAgentUrl;
    }

    public void setSlicingAgentUrl(String slicingAgentUrl) {
        this.slicingAgentUrl = slicingAgentUrl;
    }

    public List<String> getAvailableImsis() {
        return availableImsis;
    }

    public void setAvailableImsis(List<String> availableImsis) {
        this.availableImsis = availableImsis;
    }
}
