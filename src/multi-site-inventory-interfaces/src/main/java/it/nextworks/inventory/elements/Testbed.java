package it.nextworks.inventory.elements;

public enum Testbed {
    ANTWERP,
    DANUBE,
    ATHENS,
    GALATI
}
