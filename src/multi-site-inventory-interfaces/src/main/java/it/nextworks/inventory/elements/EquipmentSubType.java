package it.nextworks.inventory.elements;

public enum EquipmentSubType {
    LIBELIUM_STATION,
    AIRMAR_SS505_DEPTH_SENSOR,
    AIS_TRANSPONDER,
    ACTISENSE_DST_2_CONVERTER,
    HIKVISION_DVR_CAMERA,
    LIVOX_HORIZON,
    AXIS_CAMERA

}
